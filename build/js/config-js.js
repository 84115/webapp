({
	baseUrl: '../../js',
	name: 'build',
	out: '../../js/production.js',
	preserveLicenseComments: false,
	optimizeAllPluginResources: true,
	optimize: "uglify",
	findNestedDependencies: true,
	uglify: {
		defines: {
			DEBUG: ['name', 'false']
		}
	},
	inlineText: false,

	paths:{
		requireLib: '../js/libs/require'
	},
	include: [
		'requireLib'
	]
})