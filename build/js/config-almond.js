({
	baseUrl: '../../js',
	name: 'build',
	out: '../../js/production.js',
	preserveLicenseComments: false,
	optimizeAllPluginResources: true,
	optimize: "uglify",
	findNestedDependencies: true,
	uglify: {
		defines: {
			DEBUG: ['name', 'false']
		}
	},
	inlineText: true,

	paths:{
		requireLib: '../js/libs/almond'
	},
	include: [
		'requireLib'
	]
})