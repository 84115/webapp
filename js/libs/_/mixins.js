(function(){
	_.mixin({
		"capitalize": function(string){
			return string.substring(0, 1).toUpperCase() + string.substring(1).toLowerCase();
		},

		"summarize": function(string, length){
			length = length || 128;
			return string.substring(0, length) + "...";
		},

		"youtube": function(url, width, ops){
			width = width || "100%";
			height = "100%";
			var id = /\?v\=(\w+)/.exec(url)[1];

			return '<div class="youtube"><div class="video"><iframe'+
			' type="text/html"'+
			' width="' + width + '" height="' + height +
			' "src="http://www.youtube.com/embed/' + id + '?' + ops +
			'&amp;wmode=transparent" frameborder="0" /></div></div>';
		}

	});

})();