define(function(){
	return Backbone.Model.extend({
		defaults: {
			username: '',
			password: ''
		},
		urlRoot: "api/user"
	});
});