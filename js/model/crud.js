define(function(){
	return Backbone.Model.extend({
		defaults: {
			title: 'Barry The Frurg',
			text: '1337420',
			user_id: 0
		},
		urlRoot: "api/stamp"
	});
});