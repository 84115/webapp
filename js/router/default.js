define(["view/skeleton"], function(Skeleton){

	var skeleton = new Skeleton();

	return Backbone.Router.extend({

		initialize: function(){
			Backbone.history.start();

			this.bind("all", function(route){
				this.reset();
				if (route !== "route")
					document.title = "webapp - " + route.replace('route:', '');
			});
		},

		routes: {
			"":				"index",
			"index":		"index",
			"index/:id":	"index",
			"api":			"api",
			"api_key":		"api_key",
			"auth":			"auth",
			"crud":			"crud",
			"templateTest":	"templateTest",
			"style":		"style",
			"*path":		"notFound"
		},

		index: function(id){
			if(id === undefined || isNaN(id) === true)
				id = _.random(0, 100);

			skeleton.render.index(id);
		},

		api: function(){
			skeleton.render.api();
		},

		auth: function(username, password){
			skeleton.render.auth();
		},

		crud: function(){
			require(["model/crud", "view/crud"], function(CrudModel, CrudView){
				var CRUD_M = new CrudModel({id:0});
				CRUD_M.fetch({
					success: function(collection, response){
						skeleton.render.crud();
						var CRUD_V = new CrudView();
						CRUD_V.read(response);
					},
					error: function(){
						skeleton.render.error('500','Internal Server Error');
					}
				});
			});
		},

		templateTest: function(){
			skeleton.render.templateTest();
		},

		style: function(){
			skeleton.render.style();
		},

		notFound: function(){
			skeleton.render.notFound();
		},

		reset: function(){
			$("#content").html();//just incase a view is not render correctly
			$("#content").undelegate();//update this after default view is set
		}

	});
});