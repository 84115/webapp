define(function(){
	return Backbone.View.extend({
		render: {
			layout: {//if you append a layout template you should add the row number before the orginal id :)
				create: function(layout){
					$("#content").hide().html(layout).fadeIn(175);
				},
				lazyload: function(el, th){
					el = typeof el !== 'undefined' ? el : "img";
					th = typeof th !== 'undefined' ? th : 0;

					$(el).lazyload({
						effect: "fadeIn",
						threshold : th,
						effectspeed: 175
					});
					$(window).trigger('scroll');
				},
				block: function(){
					return '<div class="container"><div id="c1" class="sixteen columns"></div></div>';
				},
				column_two: function(){
					return '<div class="container"><div id="c1" class="two columns"></div><div id="c2" class="two columns"></div></div>';
				},
				column_three: function(){
					return '<div class="container"><div id="c1" class="one-third column"></div><div id="c2" class="one-third column"></div><div id="c3" class="one-third column"></div></div>';
				},
				column_one_two: function(){
					return '<div class="container"><div id="c1" class="one-third column"></div><div id="c2" class="two-third column"></div></div>';
				},
				column_two_one: function(){
					return '<div class="container"><div id="c1" class="two-third column"></div><div id="c2" class="one-third column"></div></div>';
				},
				column_four: function(){
					return '<div class="container"><div id="c1" class="four columns"></div><div id="c2" class="four columns"></div><div id="c3" class="four columns"></div><div id="c4" class="four columns"></div></div>';
				}
			},
			index: function(id){
				this.layout.create(this.layout.column_three);

				$("#c1").html('<h4>' + id + '</h4>');
				$("#c2").html('<h4>Features</h4><p>webapp includes the following:</p><ul><li><p><a href="http://jquery.com/"><em>jQuery</em>: Is a fast, small, and feature-rich JavaScript library. It makes things like HTML document traversal and manipulation, event handling, animation, and Ajax much simpler with an easy-to-use API that works across a multitude of browsers. With a combination of versatility and extensibility, jQuery has changed the way that millions of people write JavaScript.</a></p></li><li><p><a href="http://underscorejs.org/"><em>Underscore.js</em>: Is a utility-belt library for JavaScript that provides a lot of the functional programming support that you would expect in Prototype.js (or Ruby), but without extending any of the built-in JavaScript objects. It\'s the tie to go along with jQuery\'s tux, and Backbone.js\'s suspenders.</a></p></li><li><p><a href="http://backbonejs.org/"><em>Backbone.js</em>: Gives structure to web applications by providing models with key-value binding and custom events, collections with a rich API of enumerable functions, views with declarative event handling, and connects it all to your existing API over a RESTful JSON interface.</a></p></li><li><p><a href="http://requirejs.org/docs/start.html"><em>requirejs</em>: RequireJS is a JavaScript file and module loader. It is optimized for in-browser use, but it can be used in other JavaScript environments, like Rhino and Node. Using a modular script loader like RequireJS will improve the speed and quality of your code.</a></p></li><li><p><a href="http://qunitjs.com/"><em>QUnit</em>: QUnit is a powerful, easy-to-use JavaScript unit testing framework. It\'s used by the jQuery, jQuery UI and jQuery Mobile projects and is capable of testing any generic JavaScript code, including itself!</a></p></li><li><p><a href="http://getskeleton.com/"><em>getSkeleton</em>: Is a small collection of CSS files that can help you rapidly develop sites that look beautiful at any size, be it a 17" laptop screen or an iPhone.</a></p></li><li><p><a href="http://leaverou.github.com/prefixfree/"><em>-prefix-free</em>: Lets you use only unprefixed CSS properties everywhere. It works behind the scenes, adding the current browser’s prefix to any CSS code, only when it’s needed.</a></p></li></ul>');
				_(5).times(function(i){
					$("#c3").append('<img id="img' + i + '" data-original="img/gallery/' + i + '.jpg" src="img/grey.gif" alt="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vehicula commodo varius. Vivamus semper commodo faucibus. Aliquam vitae tortor mi. Proin turpis tellus, aliquam et varius vitae, eleifend at tortor." class="thumbnail small" /></a>');
				});
				this.layout.lazyload();
			},
			error: function(title, summary){
				this.layout.create(this.layout.block);

				$("#c1").html('<h4>' + title + '</h4><p>' + summary + '</p>');
			},
			api: function(){
				this.layout.create(this.layout.column_three);

				//$("#c1").html(_.youtube("https://www.youtube.com/watch?v=mbgWjujwvwo", "100%"));
				$("#c1").html('<h4>Methods</h4><ul><li>users</li><li>stamps</li></ul>');
				$("#c2").html('<h4>api</h4><h5>/key/</h5><i>[DELETE]</i><p>Destroy the curent users <b>api key</b></p><br /><h5>/user/</h5><i>[POST]</i><p>Lorem Ipsum</p><br /><h5>/user/:id</h5><i>[GET][PUT][DELETE]</i><p>Lorem Ipsum</p><br /><h5>/stamp/</h5><i>[POST]</i><p>Create a new stamp.</p><p><em>Required params:</em></p><ul><li>title</li><li>text</li></ul><br /><h5>/stamp/:id</h5><i>[GET][PUT][DELETE]</i><p>Lorem Ipsum</p>');
				$("#c3").html('<h4>console</h4><div id="api_console_output"></div>');
			},
			auth: function(){
				this.layout.create(this.layout.block);
				$("#c1").html('<form action="api/auth/" method="post"><fieldset>'+
					'username: <input type="text" name="username" id="username"><br />'+
					'password: <input type="password" name="password" id="password"><br />'+
					'<input type="submit" name="submit" value="Sign In">'+
				'</fieldset></form>');
			},
			style: function(){
				this.layout.create(this.layout.block);
				require(["text!template/styleGuide.tpl"], function(tpl){
					$("#c1").html(_.template(tpl)());
				});
			},
			crud: function(){
				this.layout.create(this.layout.column_three);
			},
			templateTest: function(){
				this.layout.create(this.layout.block);

				require(["text!template/userInfo.tpl"], function(tpl){
					$("#c1").html(_.template(tpl)({
						person: [
							{
								first:'<span>John</span>',
								last: 'Tron'
							},
							{
								first:'<span>Joe</span>',
								last: 'Bloggs'
							},
							{
								first:'<span>Billy</span>',
								last: 'Brown'
							}]
					}));
				});

				require(["text!template/templateTest.tpl"], function(tpl){
					$("#c1").append(_.template(tpl)({name: ['Aleksandar','Aleksander','Aleksandr','Aleksandrs','Alekzander','Alessandro','Alessio','Alex','Alexander','Alexei','Alexx','Alexzander','Alf','Alfee','Alfie','Alfred','Alfy','Alhaji','Al','Ali','Aliekber','Alieu','Alihaider','Alisdair','Alishan','Alistair','Alistar','Alister','Aliyaan','Allan','Allan','Allen','Allesandro','Allister','Ally','Alphonse','Altyiab','Alum','Alvern','Alvin','Alyas','Amaan','Aman','Amani','Ambanimoh','Ameer','Amgad','Ami','Amin','Amir','Ammaar','Ammar','Ammer','Amolpreet','Amos','Amrinder','Amrit','Amro','Anay','Andrea','Andreas','Andrei','Andrejs','Andrew','Andy','Anees','Anesu','Angel','Angelo','Angus','Anir','Anis','Anish','Anmolpreet','Annan','Anndra','Anselm','Anthony','Anthony','Antoine','Anton','Antoni','Antonio','Antony','Antonyo','Anubhav','Aodhan','Aon','Aonghus','Apisai','Arafat','Aran','Arandeep','Arann','Aray','Arayan','Archibald','Archie','Arda','Ardal','Ardeshir','Areeb','Areez','Aref','Arfin','Argyle','Argyll','Ari','Aria','Arian','Arihant','Aristomenis','Aristotelis','Arjuna','Arlo','Armaan','Arman','Armen','Arnab','Arnav','Arnold','Aron','Aronas','Arran','Arrham','Arron','Arryn','Arsalan','Artem','Arthur','Artur','Arturo','Arun','Arunas','Arved','Arya','Aryan','Aryankhan','Aryian','Aryn','Asa']}));
				});
			},
			notFound: function(){
				this.error('404', '“Not all those who wander are lost.”<br />&mdash; J.R.R. Tolkien');
			}
		}
	});
});