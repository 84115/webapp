/*	    .   oooo                                       .o8                              o8o  oooo  ooooo     ooo ooooo 
	  .o8   `888                                      "888                              `"'  `888  `888'     `8' `888' 
	.o888oo  888 .oo.   oooo  oooo  ooo. .oo.  .oo.    888oooo.  ooo. .oo.    .oooo.   oooo   888   888       8   888  
	  888    888P"Y88b  `888  `888  `888P"Y88bP"Y88b   d88' `88b `888P"Y88b  `P  )88b  `888   888   888       8   888  
	  888    888   888   888   888   888   888   888   888   888  888   888   .oP"888   888   888   888       8   888  
	  888 .  888   888   888   888   888   888   888   888   888  888   888  d8(  888   888   888   `88.    .8'   888  
	  "888" o888o o888o  `V88V"V8P' o888o o888o o888o  `Y8bod8P' o888o o888o `Y888""8o o888o o888o    `YbodP'    o888o 
	                                                                                                                   
	                                                                                                                   
	                                                                                                                   */
define(function(){
	return Backbone.View.extend({
		events: {
			"click img.thumbnail": "show",
			"click .box *": "hide",
		},
		show: function(el){
			console.log("thumbnailUI, show();", "The thumbnailUI pop has been opened.");
			var id = $(el.target).attr("id");
			var src = $(el.target).attr("src");
			var alt = $(el.target).attr("alt");

			$(".box").remove();
			$("body").css('background-color', '#bbb');
			$("body").append('<div id="thumbnail_preview_'+id+'" class="box"><img src="'+src+'"><p><em>'+alt+'</em></p>');

			$('.box').hide().fadeIn(200);
		},
		hide: function(){
			console.log("thumbnailUI, hide();", "The thumbnailUI pop has been closed.");
			$("body").css('background-color', '#fff');
			$('div').not('.box').fadeTo(200, 1);
			$(".box").fadeOut(200, function(){$(this).remove();});
		}
	});
});