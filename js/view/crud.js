define(["model/crud"], function(crudModel){
	return Backbone.View.extend({
		el: $("#content"),
		events: {
			"click #neuvo": "create",
			"blur div input.stamp_title": "update",
			"blur div input.stamp_text": "update",
			"click div .hide": "delete"
		},
		create: function(){
			var CRUD = new crudModel();
			CRUD.save();

			var CRUD_UPDATE = new crudModel({id:0});
			CRUD_UPDATE.fetch({
				success: function(collection, response){
			$("#content #c1").html('<h4 id="neuvo">CRUD</h4><p>Create, read, update &amp; delete the following:</p><div id="crud"></div>');
			for(var i = 0; i < response.length; i++){
				$("#crud").append('<fieldset>\
						<label>Title</label><input type="text" class="stamp_title" value="'+response[i].title+'">\
						<label>Text</label><input type="text" class="stamp_text" value="'+response[i].text+'">\
						<p id="'+response[i].id+'" class="hide">[x]</p>\
				</fieldset>');
			}
				}
			});
		},
		read: function(data){
			$("#content #c1").html('<h4 id="neuvo">CRUD</h4><p>Create, read, update &amp; delete the following:</p><div id="crud"></div>');
			for(var i = 0; i < data.length; i++){
				$("#crud").append('<fieldset>\
					<div id="'+data[i].id+'">\
						<label for="stamp_title">Title</label><input type="text" class="stamp_title" value="'+data[i].title+'">\
						<label for="stamp_text">Text</label><input type="text" class="stamp_text" value="'+data[i].text+'">\
						<p id="'+data[i].id+'" class="hide">[x]</p>\
					</div>\
				</fieldset>');
			}
		},
		update: function(el){
			var id = $(el.target).parent().attr("id");
			var title = $("#" + id + " > .stamp_title").val();
			var text = $("#" + id + " > .stamp_text").val();

			var CRUD = new crudModel({id:id});
			CRUD.set({title: title});
			CRUD.set({text: text});

			CRUD.save();
		},
		"delete": function(el){
			el.preventDefault();

			var id = $(el.target).attr("id");
			$( "#" + id ).parent().hide();

			var CRUD = new crudModel({id:id});
			CRUD.destroy();
		}
	});
});