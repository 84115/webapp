<h5>&#123;&#123;&#47;&#125;&#125; RAW TEXT mode</h5>
{{ _.each(person, function(person){ }}
	<li><b>first</b>: {{# person.first }}, <b>last</b>: {{# person.last }}</li>
{{ }); }}

<br />

<h5>&#123;&#123;&#35;&#125;&#125; HTML ESCAPE mode</h5>
{{ _.each(person, function(person){ }}
	<li><b>first</b>: {{/ person.first }}, <b>last</b>: {{/ person.last }}</li>
{{ }); }}

<br />

<a href="js/template/userInfo.tpl">source</a>

<br /><br />