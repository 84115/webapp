<h1 class="right">Text</h1>
<hr class="big"/>

<h1>Header 1</h1>
<h2>Header 2</h2>
<h3>Header 3</h3>
<h4>Header 4</h4>
<h5>Header 5</h5>
<h6>Header 6</h6>
<p>This is a paragraph of text about nothing... Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quis odio eget magna fermentum suscipit. Praesent molestie pulvinar purus, id pulvinar orci lacinia vitae. Curabitur enim metus, faucibus nec pharetra et, consectetur ut nisi. Nullam imperdiet posuere purus eu ullamcorper.</p>

<br />

<div class="alt">
<h1>Header 1</h1>
<h2>Header 2</h2>
<h3>Header 3</h3>
<h4>Header 4</h4>
<h5>Header 5</h5>
<h6>Header 6</h6>
<p>This text is wrapped inside of the alt class.</p>
</div>

<br />

I am <a href="#">a href</a><br />
I am <strong>strong</strong><br />
I am <em>em</em><br />
I am <b>b</b><br />
I am <i>i</i><br />
I am <mark>mark</mark><br />
I am <mark class="alt">alt-mark</mark><br />
I am <del>del</del> and I am <ins>ins</ins><br />
I am <kbd>kbd</kbd>. Press <kbd>CTRL</kbd> + <kbd>A</kbd> to use the shortcut<br />
I am <code>code</code><br />
I am <samp>samp</samp><br />
I am <var>var</var><br />
I am <sub>sub</sub><br />
I am <sup>sup</sup><br />
I am an <abbr title="The abbr tag indicates an abbreviation or an acronym, like 'WWW' or 'NATO'.">abbr</abbr>

<br />

<address>
<i class="icon user"></i> Written by <a href="mailto:webmaster@example.com">Jon Doe</a>.<br>
<i class="icon envelope"></i> Example.com<br>
<i class="icon map-marker"></i> Box 564, Disneyland, USA
</address>

<br />

<blockquote>
	<p>The <q>q</q> element represents some phrasing content quoted from another source.</p>
	<cite>Crazy hunch-backed old guy in <a href="http://en.wikipedia.org/wiki/Aladdin_(1992_Disney_film)">Aladdin</a></cite>
</blockquote>

<br />

<dl>
	<dt>The title is here</dt>
	<dd>Lorem ipsum definition goes here dolor sit amet.</dd>
	<dt>The title is here</dt>
	<dd>Lorem ipsum definition goes here dolor sit amet.</dd>
	<dt>The title is here</dt>
	<dd>Lorem ipsum definition goes here dolor sit amet.</dd>
</dl>

<pre>
import os

cwd = "c:\\conv\\"
ffmpeg = "c:\\ffmpeg\\bin\\ffmpeg.exe"

def getFLAC(dir):
  for chdir in dir:
    path = cwd + chdir
    chdir = os.listdir(path)
    outdir = os.mkdir(path + "\\out")

    print path

    for file in chdir:

      if ".flac" in file:
        newfile = file.decode("UTF-8")
        print newfile
        #the "\"" are to allow ffmpeg to handle file &amp; path names which contain spaces!
        print os.system(ffmpeg + " -i " + "\"" + path + "\\" + newfile + "\"" + " -ab 320k -ac 2 -ar 48000 " + "\"" + path + "\\out\\" + newfile.replace(".flac", "") + '.mp3' + "\"")

getFLAC(os.listdir(cwd))
</pre>

<br /><br /><br /><br /><br /><br />
<h1 class="right">Lists</h1>
<hr class="big"/>

<ul>
	<li>I am a unordered list item</li>
	<li>I am a unordered list item</li>
	<ul>
		<li>I am a 2d unordered list item</li>
		<li>I am a 2d unordered list item</li>
			<ul>
				<li>I am a 3d unordered list item</li>
				<li>I am a 3d unordered list item</li>
			</ul>
	</ul>
	<li>I am a unordered list item</li>
	<li>I am a unordered list item</li>
</ul>

<br />

<ul class="no-indent">
	<li><i class="icon folder-close"></i> I am a no-indent list item</li>
	<li><i class="icon folder-open"></i> I am a no-indent list item</li>
	<ul>
		<li><i class="icon file"></i> I am a 2d no-indent list item</li>
		<li><i class="icon file"></i> I am a 2d no-indent list item</li>
	</ul>
	<li><i class="icon folder-close"></i> I am a no-indent list item</li>
	<li><i class="icon folder-close"></i> I am a no-indent list item</li>
</ul>

<br />

<ol>
	<li>I am a ordered list item</li>
	<li>I am a ordered list item</li>
	<ol>
		<li>I am a 2d ordered list item</li>
		<li>I am a 2d ordered list item</li>
	</ol>
	<li>I am a ordered list item</li>
	<li>I am a ordered list item</li>
</ol>

<br />

<ul class="slats">
	<li class="group">
		<a href="#">
			<img src="img/thumb.png" alt="thumbnail" />
			<span class="title">This is the title</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. <span class="meta">August 10, 2011</span></p>
		</a>
	</li>
	<li class="group">
		<a href="#">
			<img src="img/thumb.png" alt="thumbnail" />
			<span class="title">This is the title</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. <span class="meta">August 10, 2011</span></p>
		</a>
	</li>
	<li class="group">
		<a href="#">
			<img src="img/thumb.png" alt="thumbnail" />
			<span class="title">This is the title</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. <span class="meta">August 10, 2011</span></p>
		</a>
	</li>
</ul>

<br /><br /><br /><br /><br /><br />
<h1 class="right">Misc</h1>
<hr class="big"/>

<nav class="breadcrumb">
	<a href="#">This is a link</a> /
	<a href="#">Dolor link two</a> /
	<strong>Active Link</strong>
</nav>

<hr />

<nav class="breadcrumb">
	<a href="#">This is a link</a> |
	<a href="#">Dolor link two</a> |
	<strong>Active Link</strong>
</nav>

<hr />

<ul class="stats-tabs">
	<li><a href="#">1,234 <span>donuts</span></a></li>
	<li><a href="#">567 <span>kayaks</span></a></li>
	<li><a href="#">23,456 <span>kittens</span></a></li>
</ul>

<hr />

<div class="pricing-table">
	<div class="color-1">
		<h3>Standard</h3>
		<h4><span class="price">£9.99</span> <span class="time">per month</span></h4>
		<ul>
			<li>5 GB Storage</li>
			<li>Free Live Support</li>
			<li>Unlimited Users</li>
			<li>No Time Tracking</li>
			<li>Enhanced SSL Security</li>
			<li class="sign-up"><a href="#" class="button medium light">Sign Up</a></li>
		</ul>
	</div>
</div>

<div class="pricing-table">
	<div class="color-2">
		<h3>Premium</h3>
		<h4><span class="price">£29.99</span> <span class="time">per month</span></h4>
		<ul>
			<li>25 GB Storage</li>
			<li>Free Live Support</li>
			<li>Unlimited Users</li>
			<li>No Time Tracking</li>
			<li>Enhanced SSL Security</li>
			<li class="sign-up"><a href="#" class="button medium gray">Sign Up</a></li>
		</ul>
	</div>
</div>

<div class="pricing-table featured">
	<div class="color-3">
		<h3>Professional</h3>
		<h4><span class="price">£49.99</span> <span class="time">per month</span></h4>
		<ul>
			<li>50 GB Storage</li>
			<li>Free Live Support</li>
			<li>Unlimited Users</li>
			<li>No Time Tracking</li>
			<li>Enhanced SSL Security</li>
			<li class="sign-up"><a href="#" class="button medium color">Sign Up</a></li>
		</ul>
	</div>
</div>

<div class="pricing-table">
	<div class="color-2">
		<h3>Ultimate</h3>
		<h4><span class="price">£99.99</span> <span class="time">per month</span></h4>
		<ul>
			<li>100 GB Storage</li>
			<li>Free Live Support</li>
			<li>Unlimited Users</li>
			<li>No Time Tracking</li>
			<li>Enhanced SSL Security</li>
			<li class="sign-up"><a href="#" class="button medium gray">Sign Up</a></li>
		</ul>
	</div>
</div>

<br /><br /><br /><br /><br /><br />
<h1 class="right">Tables</h1>
<hr class="big"/>

<table>
	<tr>
		<th>Type</th>
		<th>Date</th>
		<th>Rating</th>
	</tr>
	<tr>
		<td>Cheddar</td>
		<td>Jan 3, 2012</td>
		<td>★★★</td>
	</tr>
	<tr>
		<td>Havarti</td>
		<td>Jan 12, 2012</td>
		<td>★★★★</td>
	</tr>
	<tr>
		<td>Muenster</td>
		<td>Jan 20, 2012</td>
		<td>★★</td>
	</tr>
	<tr>
		<td>Swiss</td>
		<td>Jan 22, 2012</td>
		<td>★</td>
	</tr>
	<tr>
		<td>Gouda</td>
		<td>Jan 25, 2012</td>
		<td>★★★★★</td>
	</tr>
	<tr>
		<td>Emmentaler</td>
		<td>Jan 27, 2012</td>
		<td>★★★</td>
	</tr>
</table>

<br /><br /><br /><br /><br /><br />
<h1 class="right">Forms</h1>
<hr class="big"/>

<form>
	<fieldset>
		<label for="param1">param</label><input type="text" name="param1" id="param1">
		<label for="param2">param</label><input type="text" name="param1" id="param2">
		<p class="form-help">This is help text under the form field.</p>
	</fieldset>

	<fieldset>
		<label for="gender">Gender</label>
		<select id="gender">
			<option>Male</option>
			<option>Female</option>
			<option>Cylon</option>
		</select>
	</fieldset>

	<fieldset class="radio">
		<label for="notifications">Notifications</label>
		<ul>
			<li><label><input type="radio" name="notifications" /> Send me email</label></li>
			<li><label><input type="radio" name="notifications" /> Don't send me email</label></li>
			<li><label><input type="radio" name="notifications" /> Send me flowers</label></li>
		</ul>
	</fieldset>

	<fieldset>
		<label for="bio">Bio</label>
		<textarea id="bio"></textarea>
	</fieldset>

	<fieldset class="check">
		<label><input type="checkbox" /> I accept the terms of service and lorem ipsum.</label>
	</fieldset>

	<fieldset>
		<input type="submit" name="submit" value="Submit">
	</fieldset>
</form>

<br /><br /><br /><br /><br /><br />
<h1 class="right">Icons</h1>
<hr class="big"/>

<i class="icon glass"></i> glass<br />
<i class="icon music"></i> music<br />
<i class="icon search"></i> search<br />
<i class="icon envelope"></i> envelope<br />
<i class="icon heart"></i> heart<br />
<i class="icon star"></i> star<br />
<i class="icon star-empty"></i> star-empty<br />
<i class="icon user"></i> user<br />
<i class="icon film"></i> film<br />
<i class="icon th-large"></i> th-large<br />
<i class="icon th"></i> th<br />
<i class="icon th-list"></i> th-list<br />
<i class="icon ok"></i> ok<br />
<i class="icon remove"></i> remove<br />
<i class="icon zoom-in"></i> zoom-in<br />
<i class="icon zoom-out"></i> zoom-out<br />
<i class="icon off"></i> off<br />
<i class="icon signal"></i> signal<br />
<i class="icon cog"></i> cog<br />
<i class="icon trash"></i> trash<br />
<i class="icon home"></i> home<br />
<i class="icon file"></i> file<br />
<i class="icon time"></i> time<br />
<i class="icon road"></i> road<br />
<i class="icon download-alt"></i> download-alt<br />
<i class="icon download"></i> download<br />
<i class="icon upload"></i> upload<br />
<i class="icon inbox"></i> inbox<br />
<i class="icon play-circle"></i> play-circle<br />
<i class="icon repeat"></i> repeat<br />
<i class="icon refresh"></i> refresh<br />
<i class="icon list-alt"></i> list-alt<br />
<i class="icon lock"></i> lock<br />
<i class="icon flag"></i> flag<br />
<i class="icon headphones"></i> headphones<br />
<i class="icon volume-off"></i> volume-off<br />
<i class="icon volume-down"></i> volume-down<br />
<i class="icon volume-up"></i> volume-up<br />
<i class="icon qrcode"></i> qrcode<br />
<i class="icon barcode"></i> barcode<br />
<i class="icon tag"></i> tag<br />
<i class="icon tags"></i> tags<br />
<i class="icon book"></i> book<br />
<i class="icon bookmark"></i> bookmark<br />
<i class="icon print"></i> print<br />
<i class="icon camera"></i> camera<br />
<i class="icon font"></i> font<br />
<i class="icon bold"></i> bold<br />
<i class="icon italic"></i> italic<br />
<i class="icon text-height"></i> text-height<br />
<i class="icon text-width"></i> text-width<br />
<i class="icon align-left"></i> align-left<br />
<i class="icon align-center"></i> align-center<br />
<i class="icon align-right"></i> align-right<br />
<i class="icon align-justify"></i> align-justify<br />
<i class="icon list"></i> list<br />
<i class="icon indent-left"></i> indent-left<br />
<i class="icon indent-right"></i> indent-right<br />
<i class="icon facetime-video"></i> facetime-video<br />
<i class="icon picture"></i> picture<br />
<i class="icon pencil"></i> pencil<br />
<i class="icon map-marker"></i> map-marker<br />
<i class="icon adjust"></i> adjust<br />
<i class="icon tint"></i> tint<br />
<i class="icon edit"></i> edit<br />
<i class="icon share"></i> share<br />
<i class="icon check"></i> check<br />
<i class="icon move"></i> move<br />
<i class="icon step-backward"></i> step-backward<br />
<i class="icon fast-backward"></i> fast-backward<br />
<i class="icon backward"></i> backward<br />
<i class="icon play"></i> play<br />
<i class="icon pause"></i> pause<br />
<i class="icon stop"></i> stop<br />
<i class="icon forward"></i> forward<br />
<i class="icon fast-forward"></i> fast-forward<br />
<i class="icon step-forward"></i> step-forward<br />
<i class="icon eject"></i> eject<br />
<i class="icon chevron-left"></i> chevron-left<br />
<i class="icon chevron-right"></i> chevron-right<br />
<i class="icon plus-sign"></i> plus-sign<br />
<i class="icon minus-sign"></i> minus-sign<br />
<i class="icon remove-sign"></i> remove-sign<br />
<i class="icon ok-sign"></i> ok-sign<br />
<i class="icon question-sign"></i> question-sign<br />
<i class="icon info-sign"></i> info-sign<br />
<i class="icon screenshot"></i> screenshot<br />
<i class="icon remove-circle"></i> remove-circle<br />
<i class="icon ok-circle"></i> ok-circle<br />
<i class="icon ban-circle"></i> ban-circle<br />
<i class="icon arrow-left"></i> arrow-left<br />
<i class="icon arrow-right"></i> arrow-right<br />
<i class="icon arrow-up"></i> arrow-up<br />
<i class="icon arrow-down"></i> arrow-down<br />
<i class="icon share-alt"></i> share-alt<br />
<i class="icon resize-full"></i> resize-full<br />
<i class="icon resize-small"></i> resize-small<br />
<i class="icon plus"></i> plus<br />
<i class="icon minus"></i> minus<br />
<i class="icon asterisk"></i> asterisk<br />
<i class="icon exclamation-sign"></i> exclamation-sign<br />
<i class="icon gift"></i> gift<br />
<i class="icon leaf"></i> leaf<br />
<i class="icon fire"></i> fire<br />
<i class="icon eye-open"></i> eye-open<br />
<i class="icon eye-close"></i> eye-close<br />
<i class="icon warning-sign"></i> warning-sign<br />
<i class="icon plane"></i> plane<br />
<i class="icon calendar"></i> calendar<br />
<i class="icon random"></i> random<br />
<i class="icon magnet"></i> magnet<br />
<i class="icon chevron-up"></i> chevron-up<br />
<i class="icon chevron-down"></i> chevron-down<br />
<i class="icon retweet"></i> retweet<br />
<i class="icon shopping-cart"></i> shopping-cart<br />
<i class="icon folder-close"></i> folder-close<br />
<i class="icon folder-open"></i> folder-open<br />
<i class="icon resize-vertical"></i> resize-vertical<br />
<i class="icon resize-horizontal"></i> resize-horizontal<br />
<i class="icon hdd"></i> hdd<br />
<i class="icon bullhorn"></i> bullhorn<br />
<i class="icon bell"></i> bell<br />
<i class="icon certificate"></i> certificate<br />
<i class="icon thumbs-up"></i> thumbs-up<br />
<i class="icon thumbs-down"></i> thumbs-down<br />
<i class="icon hand-right"></i> hand-right<br />
<i class="icon hand-left"></i> hand-left<br />
<i class="icon hand-up"></i> hand-up<br />
<i class="icon hand-down"></i> hand-down<br />
<i class="icon circle-arrow-right"></i> circle-arrow-right<br />
<i class="icon circle-arrow-left"></i> circle-arrow-left<br />
<i class="icon circle-arrow-up"></i> circle-arrow-up<br />
<i class="icon circle-arrow-down"></i> circle-arrow-down<br />
<i class="icon globe"></i> globe<br />
<i class="icon wrench"></i> wrench<br />
<i class="icon tasks"></i> tasks<br />
<i class="icon filter"></i> filter<br />
<i class="icon briefcase"></i> briefcase<br />
<i class="icon fullscreen"></i> fullscreen