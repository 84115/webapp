//This and anything wrapped in DEBUG will be removed on build
if(typeof DEBUG === 'undefined') DEBUG = true;

if(DEBUG){
	test("Dependencies Loaded", function(){
		ok($, $().jquery + ": jQuery");
		ok(_, _.VERSION +": Underscore");
		ok(_.capitalize, "0.0.0: Underscore + Mixins");
		ok(Backbone, Backbone.VERSION + ": Backbone");
		ok(require, requirejs.version + ": requirejs");
		ok(test, "0.0.0: Qunit");
	});

	test("Plugins Loaded", function(){
		ok($.fn.lazyload, "jQuery Plugin: lazyload");
	});
}
else{
	//dont get me started with this :|
	requirejs.config({
		//By default load any module IDs from js/lib
		baseUrl: 'js'//,
		//except, if the module ID starts with "app",
		//load it from the js/app directory. paths
		//config is relative to the baseUrl, and
		//never includes a ".js" extension since
		//the paths config could be for a directory.
		/*paths: {
			app: '../app'
		}*/
	});
}

require(["router/default"], function(Router){
	var route = new Router();
});