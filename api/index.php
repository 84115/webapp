<?php
/*                                             o8o                     
                                               `"'                     
    oooo d8b  .ooooo.   .ooooo oo oooo  oooo  oooo  oooo d8b  .ooooo.  
    `888""8P d88' `88b d88' `888  `888  `888  `888  `888""8P d88' `88b 
     888     888ooo888 888   888   888   888   888   888     888ooo888 
     888     888    .o 888   888   888   888   888   888     888    .o 
    d888b    `Y8bod8P' `V8bod888   `V88V"V8P' o888o d888b    `Y8bod8P' 
                             888.                                      
                             8P'                                       
                             "                                         */
session_start();
header('Content-type: application/json');

require 'Slim/Middleware/PDO.php';
$db = new Database('mysql', 'localhost', 'stampit', 'root', '');

require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();









/*   o8o                    .o8                        
     `"'                   "888                        
    oooo  ooo. .oo.    .oooo888   .ooooo.  oooo    ooo 
    `888  `888P"Y88b  d88' `888  d88' `88b  `88b..8P'  
     888   888   888  888   888  888ooo888    Y888'    
     888   888   888  888   888  888    .o  .o8"'88b   
    o888o o888o o888o `Y8bod88P" `Y8bod8P' o88'   888o 
                                                       
                                                       
                                                       */
$app->get('/', function() use ($app){
	$app->redirect('../#api');
});









/*	                      o8o          oooo                                                                       
	                      `"'          `888                                                                       
	 .oooo.   oo.ooooo.  oooo           888  oooo   .ooooo.  oooo    ooo          .oooooooo  .ooooo.  ooo. .oo.   
	`P  )88b   888' `88b `888           888 .8P'   d88' `88b  `88.  .8'          888' `88b  d88' `88b `888P"Y88b  
	 .oP"888   888   888  888  8888888  888888.    888ooo888   `88..8'   8888888 888   888  888ooo888  888   888  
	d8(  888   888   888  888           888 `88b.  888    .o    `888'            `88bod8P'  888    .o  888   888  
	`Y888""8o  888bod8P' o888o         o888o o888o `Y8bod8P'     .8'             `8oooooo.  `Y8bod8P' o888o o888o 
	           888                                           .o..P'              d"     YD                        
	          o888o                                          `Y8P'               "Y88888P'                        
	                                                                                                              */
$app->get('/key/:id', function($id) use ($app){

	global $db;
	$result = $db->select("SELECT api_key FROM user WHERE id=$id");

	$today = getdate();
	$key = hash_hmac('sha256', "afheahfksdhbakufg", $today[0].'-'.$id.'b', true);
	echo '{ "api_key" : "'.base64_encode($key).'" }';
});






/*                                            
                                            
    oooo  oooo   .oooo.o  .ooooo.  oooo d8b 
    `888  `888  d88(  "8 d88' `88b `888""8P 
     888   888  `"Y88b.  888ooo888  888     
     888   888  o.  )88b 888    .o  888     
     `V88V"V8P' 8""888P' `Y8bod8P' d888b    
                                            
                                            
                                            */
//SIGN-IN
$app->post('/auth/', function() use ($app){
	$req = $app->request();
	$username = $req->params('username');
	$password = $req->params('password');

	global $db;

	$result = $db->select("SELECT username, password, api_key FROM user WHERE `username` = \"$username\" AND `password` = \"$password\" LIMIT 1");
	
	if(isset($result[0])){
		$_SESSION["api_key"] = $result[0]["api_key"];
		$app->redirect('../../');
	}
	else{
		echo json_encode(["error" => 3, "message" => "username or password is wrong" ], JSON_PRETTY_PRINT);
	}
});

//SESH
$app->get('/sesh/', function(){
	if( isset($_SESSION["api_key"]) ){
		echo json_encode(["api_key" => $_SESSION["api_key"]], JSON_PRETTY_PRINT);
	}else{
		echo json_encode(["error" => 4, "message" => "wrong key sent or no key sent or not signed in."], JSON_PRETTY_PRINT);
	}
});

//CREATE
$app->post('/user/', function() use ($app){
	$request = $app->request();
	$body = $request->getBody();
	$data = json_decode($body);

	global $db;
	$db->insert('user',[
		'username' => $data->username,
		'password' => $data->password,
	]);
});

//READ
$app->get('/user/:id', function($id){
	global $db;
	$result = $db->select("SELECT * FROM user WHERE id = $id");
	if(isset($result[0])){
		echo json_encode($result[0], JSON_PRETTY_PRINT);
	}
	else{
		echo json_encode(["error" => 2, "message" => "looks lik a typo." ], JSON_PRETTY_PRINT);
	}
});

//UPDATE
$app->put('/user/:id', function($id) use ($app){
	$request = $app->request();
	$body = $request->getBody();
	$data = json_decode($body);

	global $db;
	$db->update('user',[
		'username' => $data->username,
		'password' => $data->password,
	],"id = $id");
});

//DELETE
$app->delete('/user/:id', function($id){
	global $db;
	$db->delete('user', "id = $id");
});









/*                 .                                        
               .o8                                          
     .oooo.o .o888oo  .oooo.   ooo. .oo.  .oo.   oo.ooooo.  
    d88(  "8   888   `P  )88b  `888P"Y88bP"Y88b   888' `88b 
    `"Y88b.    888    .oP"888   888   888   888   888   888 
    o.  )88b   888 . d8(  888   888   888   888   888   888 
    8""888P'   "888" `Y888""8o o888o o888o o888o  888bod8P' 
                                                  888       
                                                 o888o      
                                                            */

//CREATE
$app->post('/stamp/', function() use ($app){
	$request = $app->request();
	$body = $request->getBody();
	$data = json_decode($body);

	global $db;
	$db->insert('stamp',[
		'title' => $data->title,
		'text' => $data->text
	]);
});

//READ
$app->get('/stamp/:id', function($id = 0){

	$end = $id + 10;

	global $db;
	$result = $db->select("SELECT title,text,id FROM stamp ORDER BY id DESC");
	echo json_encode($result, JSON_PRETTY_PRINT);
});

//UPDATE
$app->put('/stamp/:id', function($id) use ($app){
	$request = $app->request();
	$body = $request->getBody();
	$data = json_decode($body);

	global $db;
	$db->update('stamp',[
		'title' => $data->title,
		'text' => $data->text,
	],"id = $id");
});

//DELETE
$app->delete('/stamp/:id', function($id) use ($app){
	global $db;
	$db->delete('stamp', "id = $id");
});









/*     o8o               o8o      .   
     `"'               `"'    .o8   
    oooo  ooo. .oo.   oooo  .o888oo 
    `888  `888P"Y88b  `888    888   
     888   888   888   888    888   
     888   888   888   888    888 . 
    o888o o888o o888o o888o   "888" 
                                    
                                    
                                    */
$app->run();